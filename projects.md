---
title: Projects
author: bit
categories: 
  - general
created: 2025-02-09
updated: 2025-02-09T20:21:54
---


# My projects
_A collection of all my currently available public projects_

## website
_you're here!_ | [GitLab repo](https://gitlab.com/dpawensza/neocities)

## sim-cli
_a custom CLI interface\* for the algorithmic contest platform SIM_ | [GitLab repo](https://gitlab.com/maleszka/sim-cli)

## p3qe
_a custom quip editor for patapon 3_ | [GitLab repo](https://gitlab.com/dpawensza/patapon-quip-editor)

## dotfiles
_not really a project, but still a large repository that i wanted to include_ | [GitLab repo](https://gitlab.com/dpawensza/dotfiles)

## gnomefetch
_idk i was bored ok?_ | [GitLab repo](https://gitlab.com/dpawensza/gnomefetch)

## miscellaneous github contributions
_any contributions i've made to projects hosted on GitHub_ | [GitHub profile](https://github.com/Maritsu)
