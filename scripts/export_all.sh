#!/bin/bash

# safety first!
set -eu

# escape codes
BOLD=$(tput bold)
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
RESET=$(tput sgr0)

# utils
info () {
  echo -e "$1$BOLD==>$RESET ${@:2}"
}
err () {
  info $RED "[${RED}ERROR${RESET}] $@"
}

# get params
FORCE_EXPORT=0
CONVERT_ONLY=0
BLOG_ONLY=0
while getopts "fcb" flag; do
  case $flag in
    f) 
      info $RED "Flag ${RED}-f${RESET} detected, enabling ${RED}force-export${RESET}..."
      FORCE_EXPORT=1;;
    c)
      info $RED "Flag ${RED}-c${RESET} detected, disabling ${BLUE}Markdown export${RESET}..."
      CONVERT_ONLY=1;;
    b)
      info $RED "Flag ${RED}-b${RESET} detected, limiting to  ${GREEN}generating blog index${RESET} only..."
      BLOG_ONLY=1;;
    \?) ;;
  esac
done

# create db
check_db () {
  if ! test -f export.db; then
    touch export.db
    info $YELLOW "Created file checksum database at ${YELLOW}export.db${RESET}"
  else
    info $YELLOW "Database found at ${YELLOW}export.db${RESET}"
  fi
  sqlite3 export.db "create table if not exists hashes (filename TEXT PRIMARY KEY, hash INTEGER, diff_hash INTEGER);"
}

# enable force-export if template changed
check_templ_change () {
  old_templ_hash=$(sqlite3 export.db "select hash from hashes where filename='templates/default'")
  new_templ_hash=$(sha1sum templates/default.html)
  if test "$old_templ_hash" != "$new_templ_hash"; then
    if test "$old_templ_hash" = ""; then
      sqlite3 export.db "insert into hashes values ('templates/default', '$new_templ_hash', '');"
    else
      sqlite3 export.db "update hashes set hash='$(sha1sum templates/default.html)' where filename='templates/default';"
    fi
    if test $FORCE_EXPORT -ne 1; then
      FORCE_EXPORT=1
      info $RED "Template file changed, enabling ${RED}force-export${RESET}..."
    fi
  fi
}

# generate html
convert () {
  ALLGOOD=1
  FILES=$(find * -type f | grep '.md')
  for file in $FILES; do
    file=${file%.*}

    old_hash=$(sqlite3 export.db "select hash from hashes where filename='$file'")
    old_diff_hash=$(sqlite3 export.db "select diff_hash from hashes where filename='$file'")
    new_hash=$(sha1sum ${file}.md | cut -d' ' -f1)
    if test -f ${file}.diff; then
      new_diff_hash=$(sha1sum ${file}.diff | cut -d' ' -f1)
    else
      new_diff_hash=""
    fi

    if test "$file" != "blog/index" && test $FORCE_EXPORT -eq 0; then
      if test "$old_hash" = "$new_hash" && test "$old_diff_hash" = "$new_diff_hash"; then
        info $YELLOW "File ${YELLOW}${file}.md${RESET} already converted, skipping"
        continue
      fi
    fi

    info $MAGENTA "Converting ${MAGENTA}${file}.md${RESET}..."
    file_link=$(awk 'BEGIN {RS = "---"} NR==2' ${file}.md | grep -E "^link-before" | cut -d' ' -f2)
    cp templates/default.html templates/default.html.tmp -f
    if ! test -z "$file_link"; then
      info $MAGENTA "Identified include: ${MAGENTA}${file_link}${RESET}"
      sed -i "/=LINK-BEFORE=/{
      s/=LINK-BEFORE=//g
      r ${file_link}
    }" templates/default.html.tmp
  else
    sed -i '/=LINK-BEFORE=/d' templates/default.html.tmp
    fi

    if ! pandoc -s \
      $file.md \
      -o $file.html \
      --template templates/default.html.tmp \
      --resource-path $PWD
    then
      err "Failed to convert ${MAGENTA}${file}.md${RESET} to HTML"
      ALLGOOD=0
      break
    else
      info $MAGENTA "Converted ${MAGENTA}${file}.md${RESET} to ${MAGENTA}${file}.html${RESET}"
      rm templates/default.html.tmp >/dev/null
    fi

    if test -f "$file.diff"; then
      info $CYAN "Applying patch ${CYAN}${file}.diff${RESET}..."
      if ! patch -i $file.diff 2>&1; then
        err "Failed to patch ${CYAN}${file}.diff${RESET}, leaving as-is"
        ALLGOOD=0
        continue
      else
        info $CYAN "Applied patch ${CYAN}${file}.diff${RESET}"
      fi
    fi

    if test "$old_hash" = ""; then
      sqlite3 export.db "insert into hashes values ('$file', '$new_hash', '$new_diff_hash');"
    else
      sqlite3 export.db "update hashes set hash='$new_hash' where filename='$file';"
      sqlite3 export.db "update hashes set diff_hash='$new_diff_hash' where filename='$file';"
    fi
  done

  if test $ALLGOOD -eq 0; then
    info $RED "Exporting failed, see above output for more info"
  fi
}

# generate blog article list
generate_blog_index () {
  FILES=$(ls blog/ -1 -r | grep -E "\.md" | grep -vE "index")
  for file in $FILES; do
    file=${file%.*}
    
    if grep -qi "<a href=\"$file\">" blog/index.html; then
      info $YELLOW "Article ${YELLOW}${file}${RESET} already linked"
      continue
    fi

    info $GREEN "Linking article ${GREEN}${file}${RESET}..."
    title=$(grep -E "title: " blog/${file}.md | head -n1 | cut -d' ' -f2-)
    sed -i "/- placeholder -/a    <a href=\"${file}\">$title</a>" blog/index.html
    info $GREEN "Linked article ${GREEN}${file}${RESET}"
  done
}

# run everything
check_db
check_templ_change
if test $BLOG_ONLY -eq 0; then
  convert
fi
generate_blog_index
