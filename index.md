---
title: home
author: bit
categories: 
  - general
created: 2025-02-09
updated: 2025-02-11 09:14
---

# <span class="pona" sitelen-lasina="kama pona">󱤖 󱥔 </span>!!~ \^w\^
hi, welcome to my corner of the Web! :3  
this is a random website i made out of boredom and put random stuff on  
have fun exploring i guess!

# about me
i'm bit/jan Mikowa (they/it), a (mostly) self-taught competitive programmer and secondary school student.
i'm keen on all kinds of programming, fantasy literature (currently reading LoTR) and all things Linux.

# what's in here?
not much as of now, i'm still in the process of redesigning this place and adding more content.
there's a blog placeholder over at [blog/](blog/) but i haven't written anything there yet :(
